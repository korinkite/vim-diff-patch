#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''The main module that is is used to apply patch files.'''

# IMPORT STANDARD LIBRARIES
import os

# IMPORT THIRD-PARTY LIBRARIES
import vim

# IMPORT LOCAL LIBRARIES
from . import parser


def add_lines(index, lines):
    '''Add each of `lines` starting at line `index`.'''
    for line in reversed(lines):
        line = line.replace('"', '\\"')

        try:
            vim.command('call append({index}, "{line}")'.format(index=index, line=line))
        except Exception:
            raise ValueError((index, line))


def remove_lines(start, end):
    '''Delete all of the lines between `start` and `end`.'''
    vim.current.buffer[start:end] = []


def apply_patch(path):
    '''Apply a patch file directly to the current buffer.

    Warning:
        This function does no error checking. It's assumed that `path` is a
        patch file that matches with the current buffer. If that is not the
        case then the results will likely be very unpredictable.

    Args:
        path (str): The absolute path to some patch file, on-disk.

    '''
    if not os.path.isfile(path):
        vim.command('echoerr "Path \'{path}\' does not exist."'.format(path=path))
        return

    for change, lines in parser.gather_changes_from_path(path):
        if change == parser.Group.unchanged:
            continue
        elif change == parser.Group.removed:
            old_range_start = lines[0][0] - 1
            old_range_end = old_range_start + len(lines)
            remove_lines(old_range_start, old_range_end)
        elif change == parser.Group.inserted:
            new_start = lines[0][1] - 1
            add_lines(new_start, [line[2] for line in lines])
