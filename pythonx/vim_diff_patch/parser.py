#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''The module that is responsible for parsing unified diff files.'''

# IMPORT STANDARD LIBRARIES
import copy

# IMPORT THIRD-PARTY LIBRARIES
try:
    import whatthepatch
except ImportError:
    from .vendors import whatthepatch


class Group(object):

    '''A collector that knows how to merge different diff changes together.

    Feed an instance of this class a series of line-by-line diff changes it
    it will group each set of changes into a single list.

    '''

    def __init__(self):
        '''Create the properies needed to store the diff history.'''
        super(Group, self).__init__()
        self.change_history = []
        self.latest_group = []
        self._state_reference = [self.unchanged, self.inserted, self.removed]

    def _get_state(self, change):
        '''Find the type of `change` and return it.'''
        for checker in self._state_reference:
            if checker(change):
                return checker

        raise NotImplementedError('Change "{change}" type was not found.'.format(change=change))

    @staticmethod
    def inserted(change):
        '''bool: If the given `change` describes inserted text.'''
        original_line_number, patch_comparison_line_number, _ = change

        return original_line_number is None \
            and patch_comparison_line_number is not None

    @staticmethod
    def removed(change):
        '''bool: If the given `change` describes removed text.'''
        original_line_number, patch_comparison_line_number, _ = change

        return original_line_number is not None \
            and patch_comparison_line_number is None

    @staticmethod
    def unchanged(change):
        '''bool: If the given `change` describes text that wasn't editted.'''
        original_line_number, patch_comparison_line_number, _ = change

        return original_line_number is not None \
            and patch_comparison_line_number is not None

    def collapse_history(self):
        '''Take all of the recent changes and group them into a single change operation.'''
        if not self.latest_group:
            return

        state = self.latest_group[0][0]
        changes = [lines[1] for lines in self.latest_group]
        self.change_history.append((state, changes))
        self.latest_group[:] = []

    def add_change(self, change):
        '''Add the given change information to this instance.

        Args:
            change (tuple[int or NoneType, int or NoneType, str]):
                The start line of the change, the end line of the change, and
                the text that was found at that line in the diff.
                See https://pypi.org/project/whatthepatch for details.

        '''
        state = self._get_state(change)
        if not self.latest_group:
            self.latest_group.append((state, change))
            return

        latest_state, _ = self.latest_group[-1]
        if latest_state != state:
            self.collapse_history()
            self.latest_group.append((state, change))
            return

        self.latest_group.append((state, change))

    def get_hunks(self):
        '''Get every grouped change in this instance's history and return it.

        Returns:
            list[tuple[callable, list[int or NoneType, int or NoneType, str]]]:
                Every found group, a description of the change that took place
                as well as the change's line range and text.

        '''
        hunks = []

        offset = 0
        for function, lines in self.change_history:
            if function == self.unchanged:
                continue

            lines = list(lines)

            for index, (old, new, text) in enumerate(lines):
                if old is not None:
                    lines[index] = (old + offset, new, text)
                else:
                    lines[index] = (old, new, text)

                if function == self.removed:
                    offset -= 1
                elif function == self.inserted:
                    offset += 1

            hunks.append((function, lines))

        return hunks


def gather_changes_from_path(path):
    '''Get every grouped change in this instance's history and return it.

    Args:
        path (str):
            The absolute path to a diff file on-disk which will be parsed.

    Returns:
        list[tuple[callable, list[int or NoneType, int or NoneType, str]]]:
            Every found group, a description of the change that took place
            as well as the change's line range and text.

    '''
    with open(path) as handler:
        text = handler.read()

    return gather_changes(text)


def gather_changes(text):
    '''Get every grouped change in this instance's history and return it.

    Args:
        text (str):
            The text of a unified diff file which will be parsed.

    Returns:
        list[tuple[callable, list[int or NoneType, int or NoneType, str]]]:
            Every found group, a description of the change that took place
            as well as the change's line range and text.

    '''
    group = Group()

    for diff in whatthepatch.parse_patch(text):
        for change in diff.changes:
            group.add_change(change)

        group.collapse_history()

    return group.get_hunks()
