function! vim_diff_patch#apply_patch(path)
pythonx << EOF
from vim_diff_patch import patcher
patcher.apply_patch(vim.eval('a:path'))
EOF
endfunction
